# Cloudwear Social Template

This is a template that allows users to log into social and cloud services, then displays a QR code with the access token, readable in Glass using the Cloudwear SDK. Using this access token, you can query its respective social/cloud API for data access on wearable devices.

### Supported services
* Facebook
* Google
* *More soon, feel free to contribute your own*

Documentation, examples, and more services are coming.
