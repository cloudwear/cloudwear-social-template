# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  width = $(window).width()
  height = $(window).height()
  dimension = if width > height then height else width
  dimension = if dimension > 512 then 512 else dimension

  # Facebook
  fb_token = "fbk:" + $('#fb-token').data('token')
  $('#fb-token').qrcode(
    width: dimension
    height: dimension
    text: fb_token
  )

  # Google
  google_token = "ggl:" + $('#google-token').data('token')
  $('#google-token').qrcode(
    width: dimension
    height: dimension
    text: google_token
  )
