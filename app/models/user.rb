class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    user ||= User.find_by_email(auth.info.email)
    puts "###### RIGHT HERE ######"
    puts user.inspect
    if user
      user.fb_access_token = auth.credentials.token
      user.save
    else
      user = User.create(name:auth.extra.raw_info.name,
                          provider:auth.provider,
                          uid:auth.uid,
                          email:auth.info.email,
                          password:Devise.friendly_token[0,20]
                        )
      user.fb_access_token = auth.credentials.token
      user.save
    end
    user
  end

  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    if user
      user.google_access_token = access_token.credentials.token
      user.save
    else
      user = User.create(
               name: data["name"],
               email: data["email"],
               password: Devise.friendly_token[0,20]
             )
      user.google_access_token = access_token.credentials.token
      user.save
    end
    user
  end
end
